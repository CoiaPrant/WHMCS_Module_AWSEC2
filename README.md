## 使用方法

- 把 AWSEC2 目录直接扔到 WHMCS/modules/servers 下即可
- 自定义字段 cloudinit (文本框 textarea 在订单页面显示) pem (文本框 textarea 仅管理员可见) data (文本框 textarea 仅管理员可见)

## 特性

- 动态 IP (关机再开机即可更换 IP)
- 支持 AGA (貌似没啥用)
- 支持全区，多账号

## 赞助

- USDT TRC20 地址 `TNU2wK4yieGCWUxezgpZhwMHmLnRnXRtmu`
