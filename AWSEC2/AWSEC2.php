<?php

use WHMCS\Database\Capsule;
use Aws\Ec2\Ec2Client;
use Aws\Route53\Route53Client;
use Aws\GlobalAccelerator\GlobalAcceleratorClient;

function AWSEC2_ConfigOptions()
{
    if (isset($_REQUEST['id'])) {
        $params = Capsule::table('tblproducts')->where('id', $_REQUEST['id'])->first();
    }

    $ec2size = 't1.micro,t2.nano,t2.micro,t2.small,t2.medium,t2.large,t2.xlarge,t2.2xlarge,t3.nano,t3.micro,t3.small,t3.medium,t3.large,t3.xlarge,t3.2xlarge,t3a.nano,t3a.micro,t3a.small,t3a.medium,t3a.large,t3a.xlarge,t3a.2xlarge,t4g.nano,t4g.micro,t4g.small,t4g.medium,t4g.large,t4g.xlarge,t4g.2xlarge,m1.small,m1.medium,m1.large,m1.xlarge,m3.medium,m3.large,m3.xlarge,m3.2xlarge,m4.large,m4.xlarge,m4.2xlarge,m4.4xlarge,m4.10xlarge,m4.16xlarge,m2.xlarge,m2.2xlarge,m2.4xlarge,cr1.8xlarge,r3.large,r3.xlarge,r3.2xlarge,r3.4xlarge,r3.8xlarge,r4.large,r4.xlarge,r4.2xlarge,r4.4xlarge,r4.8xlarge,r4.16xlarge,r5.large,r5.xlarge,r5.2xlarge,r5.4xlarge,r5.8xlarge,r5.12xlarge,r5.16xlarge,r5.24xlarge,r5.metal,r5a.large,r5a.xlarge,r5a.2xlarge,r5a.4xlarge,r5a.8xlarge,r5a.12xlarge,r5a.16xlarge,r5a.24xlarge,r5b.large,r5b.xlarge,r5b.2xlarge,r5b.4xlarge,r5b.8xlarge,r5b.12xlarge,r5b.16xlarge,r5b.24xlarge,r5b.metal,r5d.large,r5d.xlarge,r5d.2xlarge,r5d.4xlarge,r5d.8xlarge,r5d.12xlarge,r5d.16xlarge,r5d.24xlarge,r5d.metal,r5ad.large,r5ad.xlarge,r5ad.2xlarge,r5ad.4xlarge,r5ad.8xlarge,r5ad.12xlarge,r5ad.16xlarge,r5ad.24xlarge,r6g.metal,r6g.medium,r6g.large,r6g.xlarge,r6g.2xlarge,r6g.4xlarge,r6g.8xlarge,r6g.12xlarge,r6g.16xlarge,r6gd.metal,r6gd.medium,r6gd.large,r6gd.xlarge,r6gd.2xlarge,r6gd.4xlarge,r6gd.8xlarge,r6gd.12xlarge,r6gd.16xlarge,x1.16xlarge,x1.32xlarge,x1e.xlarge,x1e.2xlarge,x1e.4xlarge,x1e.8xlarge,x1e.16xlarge,x1e.32xlarge,i2.xlarge,i2.2xlarge,i2.4xlarge,i2.8xlarge,i3.large,i3.xlarge,i3.2xlarge,i3.4xlarge,i3.8xlarge,i3.16xlarge,i3.metal,i3en.large,i3en.xlarge,i3en.2xlarge,i3en.3xlarge,i3en.6xlarge,i3en.12xlarge,i3en.24xlarge,i3en.metal,hi1.4xlarge,hs1.8xlarge,c1.medium,c1.xlarge,c3.large,c3.xlarge,c3.2xlarge,c3.4xlarge,c3.8xlarge,c4.large,c4.xlarge,c4.2xlarge,c4.4xlarge,c4.8xlarge,c5.large,c5.xlarge,c5.2xlarge,c5.4xlarge,c5.9xlarge,c5.12xlarge,c5.18xlarge,c5.24xlarge,c5.metal,c5a.large,c5a.xlarge,c5a.2xlarge,c5a.4xlarge,c5a.8xlarge,c5a.12xlarge,c5a.16xlarge,c5a.24xlarge,c5ad.large,c5ad.xlarge,c5ad.2xlarge,c5ad.4xlarge,c5ad.8xlarge,c5ad.12xlarge,c5ad.16xlarge,c5ad.24xlarge,c5d.large,c5d.xlarge,c5d.2xlarge,c5d.4xlarge,c5d.9xlarge,c5d.12xlarge,c5d.18xlarge,c5d.24xlarge,c5d.metal,c5n.large,c5n.xlarge,c5n.2xlarge,c5n.4xlarge,c5n.9xlarge,c5n.18xlarge,c5n.metal,c6g.metal,c6g.medium,c6g.large,c6g.xlarge,c6g.2xlarge,c6g.4xlarge,c6g.8xlarge,c6g.12xlarge,c6g.16xlarge,c6gd.metal,c6gd.medium,c6gd.large,c6gd.xlarge,c6gd.2xlarge,c6gd.4xlarge,c6gd.8xlarge,c6gd.12xlarge,c6gd.16xlarge,c6gn.medium,c6gn.large,c6gn.xlarge,c6gn.2xlarge,c6gn.4xlarge,c6gn.8xlarge,c6gn.12xlarge,c6gn.16xlarge,cc1.4xlarge,cc2.8xlarge,g2.2xlarge,g2.8xlarge,g3.4xlarge,g3.8xlarge,g3.16xlarge,g3s.xlarge,g4ad.4xlarge,g4ad.8xlarge,g4ad.16xlarge,g4dn.xlarge,g4dn.2xlarge,g4dn.4xlarge,g4dn.8xlarge,g4dn.12xlarge,g4dn.16xlarge,g4dn.metal,cg1.4xlarge,p2.xlarge,p2.8xlarge,p2.16xlarge,p3.2xlarge,p3.8xlarge,p3.16xlarge,p3dn.24xlarge,p4d.24xlarge,d2.xlarge,d2.2xlarge,d2.4xlarge,d2.8xlarge,d3.xlarge,d3.2xlarge,d3.4xlarge,d3.8xlarge,d3en.xlarge,d3en.2xlarge,d3en.4xlarge,d3en.6xlarge,d3en.8xlarge,d3en.12xlarge,f1.2xlarge,f1.4xlarge,f1.16xlarge,m5.large,m5.xlarge,m5.2xlarge,m5.4xlarge,m5.8xlarge,m5.12xlarge,m5.16xlarge,m5.24xlarge,m5.metal,m5a.large,m5a.xlarge,m5a.2xlarge,m5a.4xlarge,m5a.8xlarge,m5a.12xlarge,m5a.16xlarge,m5a.24xlarge,m5d.large,m5d.xlarge,m5d.2xlarge,m5d.4xlarge,m5d.8xlarge,m5d.12xlarge,m5d.16xlarge,m5d.24xlarge,m5d.metal,m5ad.large,m5ad.xlarge,m5ad.2xlarge,m5ad.4xlarge,m5ad.8xlarge,m5ad.12xlarge,m5ad.16xlarge,m5ad.24xlarge,m5zn.large,m5zn.xlarge,m5zn.2xlarge,m5zn.3xlarge,m5zn.6xlarge,m5zn.12xlarge,m5zn.metal,h1.2xlarge,h1.4xlarge,h1.8xlarge,h1.16xlarge,z1d.large,z1d.xlarge,z1d.2xlarge,z1d.3xlarge,z1d.6xlarge,z1d.12xlarge,z1d.metal,u-6tb1.metal,u-9tb1.metal,u-12tb1.metal,u-18tb1.metal,u-24tb1.metal,a1.medium,a1.large,a1.xlarge,a1.2xlarge,a1.4xlarge,a1.metal,m5dn.large,m5dn.xlarge,m5dn.2xlarge,m5dn.4xlarge,m5dn.8xlarge,m5dn.12xlarge,m5dn.16xlarge,m5dn.24xlarge,m5n.large,m5n.xlarge,m5n.2xlarge,m5n.4xlarge,m5n.8xlarge,m5n.12xlarge,m5n.16xlarge,m5n.24xlarge,r5dn.large,r5dn.xlarge,r5dn.2xlarge,r5dn.4xlarge,r5dn.8xlarge,r5dn.12xlarge,r5dn.16xlarge,r5dn.24xlarge,r5n.large,r5n.xlarge,r5n.2xlarge,r5n.4xlarge,r5n.8xlarge,r5n.12xlarge,r5n.16xlarge,r5n.24xlarge,inf1.xlarge,inf1.2xlarge,inf1.6xlarge,inf1.24xlarge,m6g.metal,m6g.medium,m6g.large,m6g.xlarge,m6g.2xlarge,m6g.4xlarge,m6g.8xlarge,m6g.12xlarge,m6g.16xlarge,m6gd.metal,m6gd.medium,m6gd.large,m6gd.xlarge,m6gd.2xlarge,m6gd.4xlarge,m6gd.8xlarge,m6gd.12xlarge,m6gd.16xlarge,mac1.metal';

    $regions = [
        'us-east-1' => '美国东部(弗吉尼亚州)',
        'us-east-2' => '美国东部(俄亥俄州)',
        'us-west-1' => '美国西部(加利福尼亚州)',
        'us-west-2' => '美国西部(俄勒冈州)',
        'af-south-1' => '非洲(开普敦)',
        'ap-east-1' => '亚洲(中国 香港)',
        'ap-south-1' => '亚洲(印度 孟买)',
        'ap-northeast-1' => '亚洲(日本 东京)',
        'ap-northeast-2' => '亚洲(韩国 首尔)',
        'ap-northeast-3' => '亚洲(日本 大阪)',
        'ap-southeast-1' => '亚洲(新加坡)',
        'ap-southeast-2' => '大洋洲(澳大利亚 悉尼)',
        'ca-central-1' => '北美洲(加拿大 中部)',
        'eu-central-1' => '欧洲(德国 法兰克福)',
        'eu-west-1' => '欧洲(爱尔兰)',
        'eu-west-2' => '欧洲(英国 伦敦)',
        'eu-south-1' => '欧洲(意大利 米兰)',
        'eu-west-3' => '欧洲(法国 巴黎)',
        'eu-north-1' => '欧洲(瑞典 斯德哥尔摩)',
        'me-south-1' => '中东(巴林)',
        'sa-east-1' => '南美洲(巴西 圣保罗)',
    ];

    $systems = [
        0 => '用户自行选择',
        1 => 'Amazon Linux 2',
        2 => 'Red Hat Enterprise Linux 8',
        3 => 'SUSE Linux Enterprise Server 15 SP2',
        4 => 'Ubuntu Server 20.04 LTS',
        5 => 'Ubuntu Server 18.04 LTS',
        6 => 'Windows 2019 Datacenter',
        7 => 'Debian 10',
        8 => 'Windows Server 2016',
        9 => 'SUSE Linux Enterprise Server 12 SP5',
        10 => 'Microsoft Windows Server 2012 R2',
        11 => 'Ubuntu Server 16.04 LTS',
    ];

    $systems_ARM64 = [
        0 => '用户自行选择',
        1 => 'Amazon Linux 2',
        2 => 'Red Hat Enterprise Linux 8',
        3 => 'SUSE Linux Enterprise Server 15 SP2',
        4 => 'Ubuntu Server 20.04 LTS',
        5 => 'Ubuntu Server 18.04 LTS',
        7 => 'Debian 10',
        11 => 'Ubuntu Server 16.04 LTS',
    ];

    if (empty($params->configoption5)) {
        unset($regions);
        unset($systems);
        $regions[] = '请先填完其他信息后保存';
        $systems[] = '请先填完其他信息后保存';
    } else if ($params->configoption5 == 'ARM64') {
        unset($regions['af-south-1']);
        unset($regions['ap-northeast-3']);
    }

    $configarray = [
        'Access Key ID'                => ['Type' => 'text', 'Description' => 'AWS帐号安全设置 <a href="https://console.aws.amazon.com/iam/home#/security_credentials">点此打开</a>'],                //1
        'Secret Access Key'                => ['Type' => 'text'],                //2
        //3
        '实例区域'                    => ['Type' => 'dropdown', 'Options' => $regions, 'Description' => '具体登陆AWS控制台查看'], //3
        '实例大小'                    => ['Type' => 'dropdown', 'Options' => $ec2size, 'Description' => '具体登陆AWS控制台查看'], //4
        '机器架构'                 => ['Type' => 'dropdown', 'Options' => "x86_64,ARM64"], //5
        '操作系统'                 => ['Type' => 'dropdown', 'Options' => $params->configoption5 == "ARM64" ? $systems_ARM64 : $systems],  //6
        '全球加速网络(Global Accelerator)' => ['Type' => 'dropdown', 'Options' => ['false' => '关', 'true' => '开']], //7
        '开关机功能' => ['Type' => 'dropdown', 'Options' => ['disable' => '禁用', 'enable' => '启用']], //8
        '自定义前缀'                => ['Type' => 'text'],                //9

    ];

    unset($systems[0]);
    if (!Capsule::table('tblproductconfiggroups')->where('name', 'AWSEC2')->exists()) {
        $currencies = Capsule::table("tblcurrencies")->get();
        $cgid = Capsule::table('tblproductconfiggroups')->insertGetId(['name' => 'AWSEC2', 'description' => 'By AWSEC2 modules']);
        $cid = Capsule::table('tblproductconfigoptions')->insertGetId(['gid' => $cgid, 'optionname' => 'OS', 'optiontype' => '1', 'qtyminimum' => 0, 'qtymaximum' => 0, 'order' => 0, 'hidden' => 0]);
        foreach ($systems as $id => $display) {
            $rid = Capsule::table('tblproductconfigoptionssub')->insertGetId(['configid' => $cid, 'optionname' => $id . '|' . $display, 'sortorder' => 0, 'hidden' => 0]);
            foreach ($currencies as $currency) {
                Capsule::table('tblpricing')->insert(['type' => 'configoptions', 'currency' => $currency->id, 'relid' => $rid, 'msetupfee' => 0.00, 'qsetupfee' => 0.00, 'ssetupfee' => 0.00, 'asetupfee' => 0.00, 'bsetupfee' => 0.00, 'tsetupfee' => 0.00, 'monthly' => 0.00, 'monthly' => 0.00, 'semiannually' => 0.00, 'annually' => 0.00, 'biennially' => 0.00, 'triennially' => 0.00]);
            }
        }
    }

    unset($systems_ARM64[0]);
    if (!Capsule::table('tblproductconfiggroups')->where('name', 'AWSEC2_ARM64')->exists()) {
        $currencies = Capsule::table("tblcurrencies")->get();
        $cgid = Capsule::table('tblproductconfiggroups')->insertGetId(['name' => 'AWSEC2_ARM64', 'description' => 'By AWSEC2 modules']);
        $cid = Capsule::table('tblproductconfigoptions')->insertGetId(['gid' => $cgid, 'optionname' => 'OS', 'optiontype' => '1', 'qtyminimum' => 0, 'qtymaximum' => 0, 'order' => 0, 'hidden' => 0]);
        foreach ($systems_ARM64 as $id => $display) {
            $rid = Capsule::table('tblproductconfigoptionssub')->insertGetId(['configid' => $cid, 'optionname' => $id . '|' . $display, 'sortorder' => 0, 'hidden' => 0]);
            foreach ($currencies as $currency) {
                Capsule::table('tblpricing')->insert(['type' => 'configoptions', 'currency' => $currency->id, 'relid' => $rid, 'msetupfee' => 0.00, 'qsetupfee' => 0.00, 'ssetupfee' => 0.00, 'asetupfee' => 0.00, 'bsetupfee' => 0.00, 'tsetupfee' => 0.00, 'monthly' => 0.00, 'monthly' => 0.00, 'semiannually' => 0.00, 'annually' => 0.00, 'biennially' => 0.00, 'triennially' => 0.00]);
            }
        }
    }
    return $configarray;
}

function AWSEC2_CreateAccount(array $params)
{
    $os = $params['configoption6'];
    if ($os == 0) {
        $os = $params['configoptions']['OS'];
    }

    try {
        $ec2 = new Ec2Client([
            'region' => $params['configoption3'],
            'version' => '2016-11-15',
            'credentials' => [
                'key' => $params['configoption1'],
                'secret' => $params['configoption2']
            ],
        ]);

        $key = $ec2->createKeyPair([
            'DryRun' => false,
            'KeyName' => $params['configoption9'] . $params['serviceid'], // REQUIRED
        ]);

        AWSEC2_setCustomfieldsValue($params, 'pem', $key['KeyMaterial']);

        $machine = $ec2->runInstances([
            'DisableApiTermination' => false,
            'DryRun' => false,
            'EbsOptimized' => false,
            'ImageId' => AWSEC2_GetAMIID($params['configoption3'], $os, $params['configoption5'] == 'ARM64' ? true : false),
            'InstanceInitiatedShutdownBehavior' => 'stop',
            'InstanceType' => $params['configoption4'],
            'KeyName' => $params['configoption9'] . $params['serviceid'],
            'MaxCount' => 1, // REQUIRED
            'MinCount' => 1, // REQUIRED
            'NetworkInterfaces' => [
                [
                    'AssociateCarrierIpAddress' => false,
                    'AssociatePublicIpAddress' => true,
                    'DeleteOnTermination' => true,
                    'DeviceIndex' => 0,
                    'InterfaceType' => 'interface',
                    'NetworkCardIndex' => 0,
                ],
            ],
            'UserData' => $params['customfields']['cloudinit'],
        ]);
        $data['vm'] = $machine['Instances'][0]['InstanceId'];
        AWSEC2_setCustomfieldsValue($params, 'data', json_encode($data));

        $nsg = $ec2->createSecurityGroup([
            'Description' =>  $params['configoption9'] . $params['serviceid'], // REQUIRED
            'DryRun' => false,
            'GroupName' => $params['configoption9'] . $params['serviceid'], // REQUIRED
            'VpcId' =>  $machine['Instances'][0]['NetworkInterfaces'][0]['VpcId'],
        ]);

        $data['nsg'] = $nsg['GroupId'];
        AWSEC2_setCustomfieldsValue($params, 'data', json_encode($data));

        $ec2->modifyNetworkInterfaceAttribute([
            'DryRun' => false,
            'Groups' => [$nsg['GroupId']],
            'NetworkInterfaceId' => $machine['Instances'][0]['NetworkInterfaces'][0]['NetworkInterfaceId'],
        ]);


        $data['nic'] = $machine['Instances'][0]['NetworkInterfaces'][0]['NetworkInterfaceId'];
        AWSEC2_setCustomfieldsValue($params, 'data', json_encode($data));

        $ec2->authorizeSecurityGroupIngress([
            'DryRun' => false,
            'GroupId' => $nsg['GroupId'],
            'IpPermissions' => [
                [

                    'FromPort' => 1,
                    'ToPort' => 65535,
                    'IpProtocol' => '-1',
                    'IpRanges' => [
                        [
                            'CidrIp' => '0.0.0.0/0',
                            'Description' => 'All ipv4 address. For: ' . $params['configoption9'] . $params['serviceid'],
                        ],
                    ],
                    'Ipv6Ranges' => [
                        [
                            'FromPort' => 1,
                            'ToPort' => 65535,
                            'IpProtocol' => '-1',
                            'CidrIpv6' => '::/0',
                            'Description' => 'All ipv6 address. For: ' . $params['configoption9'] . $params['serviceid'],
                        ],
                    ],
                ],

            ],
        ]);


        $eth = $ec2->describeNetworkInterfaces([
            'DryRun' => false,
            'NetworkInterfaceIds' => [$machine['Instances'][0]['NetworkInterfaces'][0]['NetworkInterfaceId']],
        ]);
        Capsule::table('tblhosting')->where('id', $params['serviceid'])->update(['dedicatedip' => $eth['NetworkInterfaces'][0]['Association']['PublicIp']]);

        if ($params['configoption7'] == 'true') {
            $gac = new GlobalAcceleratorClient([
                'region' => 'us-west-2',
                'version' => '2018-08-08',
                'credentials' => [
                    'key' => $params['configoption1'],
                    'secret' => $params['configoption2']
                ],
            ]);

            $ga = $gac->createAccelerator([
                'Enabled' => true,
                'IdempotencyToken' => $params['configoption9'] . $params['serviceid'] . rand(1, time()), // REQUIRED
                'IpAddressType' => 'IPV4',
                'Name' => $params['configoption9'] . $params['serviceid'], // REQUIRED
            ]);


            Capsule::table('tblhosting')->where('id', $params['serviceid'])->update(['domain' => $ga['Accelerator']['DnsName']]);

            $data['aga'] = $ga['Accelerator']['AcceleratorArn'];
            AWSEC2_setCustomfieldsValue($params, 'data', json_encode($data));

            $tcp = $gac->createListener([
                'AcceleratorArn' => $ga['Accelerator']['AcceleratorArn'], // REQUIRED
                'ClientAffinity' => 'SOURCE_IP',
                'IdempotencyToken' => $params['serviceid'] . rand(1, time()) . $params['configoption9'], // REQUIRED
                'PortRanges' => [ // REQUIRED
                    [
                        'FromPort' => 1,
                        'ToPort' => 65535,
                    ],
                ],
                'Protocol' => 'TCP', // REQUIRED
            ]);

            $udp = $gac->createListener([
                'AcceleratorArn' => $ga['Accelerator']['AcceleratorArn'], // REQUIRED
                'ClientAffinity' => 'SOURCE_IP',
                'IdempotencyToken' => $params['serviceid'] . $params['configoption9'] . rand(1, time()), // REQUIRED
                'PortRanges' => [ // REQUIRED
                    [
                        'FromPort' => 1,
                        'ToPort' => 65535,
                    ],
                ],
                'Protocol' => 'UDP', // REQUIRED
            ]);

            $gac->createEndpointGroup([
                'EndpointConfigurations' => [
                    [
                        'ClientIPPreservationEnabled' => true,
                        'EndpointId' => $machine['Instances'][0]['InstanceId'],
                        'Weight' => 128,
                    ],
                ],
                'EndpointGroupRegion' => $params['configoption3'], // REQUIRED
                'HealthCheckIntervalSeconds' => 30,
                'HealthCheckPort' => 22,
                'HealthCheckProtocol' => 'TCP',
                'IdempotencyToken' => $params['configoption9'] . rand(1, rand(100, 200)) . $params['serviceid'] . rand(1, time()), // REQUIRED
                'ListenerArn' => $tcp['Listener']['ListenerArn'], // REQUIRED
            ]);

            $gac->createEndpointGroup([
                'EndpointConfigurations' => [
                    [
                        'ClientIPPreservationEnabled' => true,
                        'EndpointId' => $machine['Instances'][0]['InstanceId'],
                        'Weight' => 128,
                    ],
                ],
                'EndpointGroupRegion' => $params['configoption3'], // REQUIRED
                'HealthCheckIntervalSeconds' => 30,
                'HealthCheckPort' => 22,
                'HealthCheckProtocol' => 'TCP',
                'IdempotencyToken' => $params['serviceid'] . rand(1, rand(125, 225)) . $params['configoption9'] . rand(1, time()), // REQUIRED
                'ListenerArn' => $udp['Listener']['ListenerArn'], // REQUIRED
            ]);

            $route53 = new Route53Client([
                'region' => 'us-east-1',
                'version' => '2013-04-01',
                'credentials' => [
                    'key' => $params['configoption1'],
                    'secret' => $params['configoption2']
                ],
            ]);

            $check = $route53->createHealthCheck([
                'CallerReference' => $params['configoption9'] . $params['serviceid'] . time(), // REQUIRED
                'HealthCheckConfig' => [ // REQUIRED
                    'Disabled' => false,
                    'IPAddress' => $eth['NetworkInterfaces'][0]['Association']['PublicIp'],
                    'Inverted' => false,
                    'MeasureLatency' => false,
                    'Port' => 22,
                    'Type' => 'TCP', // REQUIRED
                ],
            ]);

            $data['check'] = $check['HealthCheck']['Id'];
            AWSEC2_setCustomfieldsValue($params, 'data', json_encode($data));
        }
        AWSEC2_setCustomfieldsValue($params, 'data', json_encode($data));
        return 'success';
    } catch (\Exception $e) {
        return $e->getMessage();
    }
}

function AWSEC2_SuspendAccount(array $params)
{
    return AWSEC2_shutdown($params);
}


function AWSEC2_UnsuspendAccount(array $params)
{
    return AWSEC2_boot($params);
}


function AWSEC2_TerminateAccount(array $params)
{

    try {
        $data = json_decode($params['customfields']['data'], true);

        $ec2 = new Ec2Client([
            'region' => $params['configoption3'],
            'version' => '2016-11-15',
            'credentials' => [
                'key' => $params['configoption1'],
                'secret' => $params['configoption2']
            ],
        ]);

        $ec2->deleteKeyPair([
            'KeyName' => $params['configoption9'] . $params['serviceid']
        ]);

        $ec2->terminateInstances([
            'DryRun' => false,
            'InstanceIds' => [$data['vm']], // REQUIRED
        ]);
        AWSEC2_disable_aga($params);

        sleep(30);

        $ec2->deleteSecurityGroup([
            'DryRun' => false,
            'GroupId' => $data['nsg'],
        ]);


        AWSEC2_setCustomfieldsValue($params, 'pem', '');
        AWSEC2_setCustomfieldsValue($params, 'data', '');
        return 'success';
    } catch (\Exception $e) {
        return $e->getMessage();
    }
}


function AWSEC2_AdminCustomButtonArray()
{
    return [
        '开机' => 'boot',
        '重启' => 'reboot',
        '关机' => 'shutdown',
        '启用AGA' => 'enable_aga',
        '关闭AGA' => 'disable_aga',
        '删除私钥' => 'delete_pem',
        '删除安全组' => 'delete_nsg',
    ];
}

function AWSEC2_ClientAreaCustomButtonArray()
{
    return [
        '开机' => 'boot',
        '重启' => 'reboot',
        '关机' => 'shutdown',
    ];
}


function AWSEC2_ClientArea(array $params)
{
    if ($params['status'] != 'Active') {
        return;
    }

    $data = json_decode($params['customfields']['data'], true);
    if ($_REQUEST['do'] == 'getpem') {
        Header("Content-type: application/octet-stream");
        Header("Accept-Ranges: bytes");
        Header("Accept-Length: " . strlen($params['customfields']['pem']));
        Header("Content-Disposition: attachment; filename=ssh.pem");
        exit($params['customfields']['pem']);
    }

    $systems = [
        0 => '用户自行选择',
        1 => 'Amazon Linux 2',
        2 => 'Red Hat Enterprise Linux 8',
        3 => 'SUSE Linux Enterprise Server 15 SP2',
        4 => 'Ubuntu Server 20.04 LTS',
        5 => 'Ubuntu Server 18.04 LTS',
        6 => 'Windows 2019 Datacenter',
        7 => 'Debian 10',
        8 => 'Windows Server 2016',
        9 => 'SUSE Linux Enterprise Server 12 SP5',
        10 => 'Microsoft Windows Server 2012 R2',
        11 => 'Ubuntu Server 16.04 LTS',
    ];

    try {
        $ec2 = new Ec2Client([
            'region' => $params['configoption3'],
            'version' => '2016-11-15',
            'credentials' => [
                'key' => $params['configoption1'],
                'secret' => $params['configoption2']
            ],
        ]);

        $eth = $ec2->describeNetworkInterfaces([
            'DryRun' => false,
            'NetworkInterfaceIds' => [$data['nic']],
        ]);

        Capsule::table('tblhosting')->where('id', $params['serviceid'])->update(['dedicatedip' => $eth['NetworkInterfaces'][0]['Association']['PublicIp']]);

        if (isset($data['check'])) {
            $route53 = new Route53Client([
                'region' => 'us-east-1',
                'version' => '2013-04-01',
                'credentials' => [
                    'key' => $params['configoption1'],
                    'secret' => $params['configoption2']
                ],
            ]);

            $route53->updateHealthCheck([
                'HealthCheckId' => $data['check'],
                'Disabled' => false,
                'IPAddress' => $eth['NetworkInterfaces'][0]['Association']['PublicIp'],
                'Inverted' => false,
                'Port' => 22,
            ]);
        }

        return [
            'tabOverviewReplacementTemplate' => 'clientarea.tpl',
            'vars' => [
                'system' => $systems[$params['configoption6'] == 0 ?  $params['configoptions']['OS'] : $params['configoption6']],
                'ip'    => $eth['NetworkInterfaces'][0]['Association']['PublicIp'],
                'domain' => $params['domain'],
            ],
        ];
    } catch (\Exception $e) {
        return "<h1>服务不可用</h1>";
    }
}

function AWSEC2_shutdown(array $params)
{


    if ($params['configoption8'] != 'enable') {
        return '管理员已关闭此功能';
    }

    try {

        $data = json_decode($params['customfields']['data'], true);
        $ec2 = new Ec2Client([
            'region' => $params['configoption3'],
            'version' => '2016-11-15',
            'credentials' => [
                'key' => $params['configoption1'],
                'secret' => $params['configoption2']
            ],
        ]);
        $ec2->stopInstances([
            'DryRun' => false,
            'InstanceIds' => [$data['vm']], // REQUIRED
        ]);
        return 'success';
    } catch (\Exception $e) {
        return $e->getMessage();
    }
}


function AWSEC2_boot(array $params)
{

    if ($params['configoption8'] != 'enable') {
        return '管理员已关闭此功能';
    }

    try {

        $data = json_decode($params['customfields']['data'], true);
        $ec2 = new Ec2Client([
            'region' => $params['configoption3'],
            'version' => '2016-11-15',
            'credentials' => [
                'key' => $params['configoption1'],
                'secret' => $params['configoption2']
            ],
        ]);

        $ec2->startInstances([
            'DryRun' => false,
            'InstanceIds' => [$data['vm']], // REQUIRED
        ]);
        sleep(5);

        $eth = $ec2->describeNetworkInterfaces([
            'DryRun' => false,
            'NetworkInterfaceIds' => [$data['nic']],
        ]);

        Capsule::table('tblhosting')->where('id', $params['serviceid'])->update(['dedicatedip' => $eth['NetworkInterfaces'][0]['Association']['PublicIp']]);

        if (isset($data['check'])) {
            $route53 = new Route53Client([
                'region' => 'us-east-1',
                'version' => '2013-04-01',
                'credentials' => [
                    'key' => $params['configoption1'],
                    'secret' => $params['configoption2']
                ],
            ]);

            $check = $route53->updateHealthCheck([
                'HealthCheckId' => $data['check'],
                'Disabled' => false,
                'IPAddress' => $eth['NetworkInterfaces'][0]['Association']['PublicIp'],
                'Inverted' => false,
                'Port' => 22,
            ]);
        }
        return 'success';
    } catch (\Exception $e) {
        return $e->getMessage();
    }
}


function AWSEC2_reboot(array $params)
{


    if ($params['configoption8'] != 'enable') {
        return '管理员已关闭此功能';
    }

    try {

        $data = json_decode($params['customfields']['data'], true);
        $ec2 = new Ec2Client([
            'region' => $params['configoption3'],
            'version' => '2016-11-15',
            'credentials' => [
                'key' => $params['configoption1'],
                'secret' => $params['configoption2']
            ],
        ]);

        $ec2->rebootInstances([
            'DryRun' => false,
            'InstanceIds' => [$data['vm']], // REQUIRED
        ]);
        return 'success';
    } catch (\Exception $e) {
        return $e->getMessage();
    }
}


function AWSEC2_enable_aga(array $params)
{
    try {
        $data = json_decode($params['customfields']['data'], true);

        $ec2 = new Ec2Client([
            'region' => $params['configoption3'],
            'version' => '2016-11-15',
            'credentials' => [
                'key' => $params['configoption1'],
                'secret' => $params['configoption2']
            ],
        ]);

        $eth = $ec2->describeNetworkInterfaces([
            'DryRun' => false,
            'NetworkInterfaceIds' => [$data['nic']],
        ]);


        $gac = new GlobalAcceleratorClient([
            'region' => 'us-west-2',
            'version' => '2018-08-08',
            'credentials' => [
                'key' => $params['configoption1'],
                'secret' => $params['configoption2']
            ],
        ]);

        $ga = $gac->createAccelerator([
            'Enabled' => true,
            'IdempotencyToken' => $params['configoption9'] . $params['serviceid'] . rand(1, time()), // REQUIRED
            'IpAddressType' => 'IPV4',
            'Name' => (string)$params['serviceid'], // REQUIRED
        ]);


        Capsule::table('tblhosting')->where('id', $params['serviceid'])->update(['domain' => $ga['Accelerator']['DnsName']]);

        $data['aga'] = $ga['Accelerator']['AcceleratorArn'];

        $tcp = $gac->createListener([
            'AcceleratorArn' => $ga['Accelerator']['AcceleratorArn'], // REQUIRED
            'ClientAffinity' => 'SOURCE_IP',
            'IdempotencyToken' => $params['serviceid'] . rand(1, time()) . $params['configoption9'], // REQUIRED
            'PortRanges' => [ // REQUIRED
                [
                    'FromPort' => 1,
                    'ToPort' => 65535,
                ],
            ],
            'Protocol' => 'TCP', // REQUIRED
        ]);

        $udp = $gac->createListener([
            'AcceleratorArn' => $ga['Accelerator']['AcceleratorArn'], // REQUIRED
            'ClientAffinity' => 'SOURCE_IP',
            'IdempotencyToken' => $params['serviceid'] . $params['configoption9'] . rand(1, time()), // REQUIRED
            'PortRanges' => [ // REQUIRED
                [
                    'FromPort' => 1,
                    'ToPort' => 65535,
                ],
            ],
            'Protocol' => 'UDP', // REQUIRED
        ]);

        $gac->createEndpointGroup([
            'EndpointConfigurations' => [
                [
                    'ClientIPPreservationEnabled' => true,
                    'EndpointId' => $data['vm'],
                    'Weight' => 128,
                ],
            ],
            'EndpointGroupRegion' => $params['configoption3'], // REQUIRED
            'HealthCheckIntervalSeconds' => 30,
            'HealthCheckPort' => 22,
            'HealthCheckProtocol' => 'TCP',
            'IdempotencyToken' => $params['configoption9'] . rand(1, rand(100, 200)) . $params['serviceid'] . rand(1, time()), // REQUIRED
            'ListenerArn' => $tcp['Listener']['ListenerArn'], // REQUIRED
        ]);

        $gac->createEndpointGroup([
            'EndpointConfigurations' => [
                [
                    'ClientIPPreservationEnabled' => true,
                    'EndpointId' => $data['vm'],
                    'Weight' => 128,
                ],
            ],
            'EndpointGroupRegion' => $params['configoption3'], // REQUIRED
            'HealthCheckIntervalSeconds' => 30,
            'HealthCheckPort' => 22,
            'HealthCheckProtocol' => 'TCP',
            'IdempotencyToken' => $params['serviceid'] . rand(1, rand(125, 225)) . $params['configoption9'] . rand(1, time()), // REQUIRED
            'ListenerArn' => $udp['Listener']['ListenerArn'], // REQUIRED
        ]);

        $route53 = new Route53Client([
            'region' => 'us-east-1',
            'version' => '2013-04-01',
            'credentials' => [
                'key' => $params['configoption1'],
                'secret' => $params['configoption2']
            ],
        ]);

        $check = $route53->createHealthCheck([
            'CallerReference' => $params['configoption9'] . $params['serviceid'] . time(), // REQUIRED
            'HealthCheckConfig' => [ // REQUIRED
                'Disabled' => false,
                'IPAddress' => $eth['NetworkInterfaces'][0]['Association']['PublicIp'],
                'Inverted' => false,
                'MeasureLatency' => false,
                'Port' => 22,
                'Type' => 'TCP', // REQUIRED
            ],
        ]);

        $data['check'] = $check['HealthCheck']['Id'];
        AWSEC2_setCustomfieldsValue($params, 'data', json_encode($data));
        return 'success';
    } catch (\Exception $e) {
        return $e->getMessage();
    }
}

function AWSEC2_disable_aga(array $params)
{
    try {

        $data = json_decode($params['customfields']['data'], true);

        if (isset($data['check'])) {
            $route53 = new Route53Client([
                'region' => 'us-east-1',
                'version' => '2013-04-01',
                'credentials' => [
                    'key' => $params['configoption1'],
                    'secret' => $params['configoption2']
                ],
            ]);
            $route53->deleteHealthCheck([
                'HealthCheckId' => $data['check'], // REQUIRED
            ]);
            unset($data['check']);
            AWSEC2_setCustomfieldsValue($params, 'data', json_encode($data));
        }


        if (isset($data['aga'])) {

            $gac = new GlobalAcceleratorClient([
                'region' => 'us-west-2',
                'version' => '2018-08-08',
                'credentials' => [
                    'key' => $params['configoption1'],
                    'secret' => $params['configoption2']
                ],
            ]);

            $gac->updateAccelerator([
                'AcceleratorArn' => $data['aga'], // REQUIRED
                'Enabled' => false,
                'IpAddressType' => 'IPV4',
            ]);

            $listeners = $gac->listListeners([
                'AcceleratorArn' => $data['aga']
            ]);

            foreach ($listeners['Listeners'] as $listener) {
                $endgroups = $gac->listEndpointGroups([
                    'ListenerArn' => $listener['ListenerArn'],
                ]);
                foreach ($endgroups['EndpointGroups'] as $endgroup) {
                    $gac->deleteEndpointGroup([
                        'EndpointGroupArn' => $endgroup['EndpointGroupArn'],
                    ]);
                }

                $gac->deleteListener([
                    'ListenerArn' => $listener['ListenerArn'],
                ]);
            }

            sleep(30);

            $gac->deleteAccelerator([
                'AcceleratorArn' => $data['aga'], // REQUIRED
            ]);
            unset($data['aga']);
            Capsule::table('tblhosting')->where('id', $params['serviceid'])->update(['domain' => '']);
            AWSEC2_setCustomfieldsValue($params, 'data', json_encode($data));
        }



        AWSEC2_setCustomfieldsValue($params, 'data', json_encode($data));
        return 'success';
    } catch (\Exception $e) {
        return $e->getMessage();
    }
}

function AWSEC2_delete_pem(array $params)
{
    try {
        $ec2 = new Ec2Client([
            'region' => $params['configoption3'],
            'version' => '2016-11-15',
            'credentials' => [
                'key' => $params['configoption1'],
                'secret' => $params['configoption2']
            ],
        ]);

        $ec2->deleteKeyPair([
            'KeyName' => $params['configoption9'] . $params['serviceid']
        ]);
        AWSEC2_setCustomfieldsValue($params, 'pem', '');
        return 'success';
    } catch (\Exception $e) {
        return $e->getMessage();
    }
}

function AWSEC2_delete_nsg(array $params)
{
    try {
        $data = json_decode($params['customfields']['data'], true);
        $ec2 = new Ec2Client([
            'region' => $params['configoption3'],
            'version' => '2016-11-15',
            'credentials' => [
                'key' => $params['configoption1'],
                'secret' => $params['configoption2']
            ],
        ]);

        $ec2->deleteSecurityGroup([
            'DryRun' => false,
            'GroupId' => $data['nsg'],
        ]);


        AWSEC2_setCustomfieldsValue($params, 'pem', '');
        AWSEC2_setCustomfieldsValue($params, 'data', '');
        return 'success';
    } catch (\Exception $e) {
        return $e->getMessage();
    }
}

function AWSEC2_setCustomfieldsValue(array $params, string $field, string $value)
{

    $res = Capsule::table('tblcustomfields')->where('relid', $params['pid'])->where('fieldname', $field)->first();
    if ($res) {
        $fieldValue = Capsule::table('tblcustomfieldsvalues')->where('relid', $params['serviceid'])->where('fieldid', $res->id)->first();
        if ($fieldValue) {
            if ($fieldValue->value !== $value) {
                Capsule::table('tblcustomfieldsvalues')
                    ->where('relid', $params['serviceid'])
                    ->where('fieldid', $res->id)
                    ->update(
                        [
                            'value' => $value,
                        ]
                    );
            }
        } else {
            Capsule::table('tblcustomfieldsvalues')
                ->insert(
                    [
                        'relid'   => $params['serviceid'],
                        'fieldid' => $res->id,
                        'value'   => $value,
                    ]
                );
        }
    }
}

function AWSEC2_GetAMIID(string $region, int $index, bool $is_ARM64)
{
    /*
        1 Amazon Linux 2
        2 Red Hat Enterprise Linux 8
        3 SUSE Linux Enterprise Server 15 SP2
        4 Ubuntu Server 20.04 LTS
        5 Ubuntu Server 18.04 LTS
        6 Windows 2019 Datacenter
        7 Debian 10
        8 Windows Server 2016
        9 SUSE Linux Enterprise Server 12 SP5
        10 Microsoft Windows Server 2012 R2
        11 Ubuntu Server 16.04 LTS
    */

    $amis_amd64 = [
        'us-east-1' => [
            1 => '0c2b8ca1dad447f8a',
            2 => '0b0af3577fe5e3532',
            3 => '0fde50fcbcd46f2f7',
            4 => '09e67e426f25ce0d7',
            5 => '0747bdcabd34c712a',
            6 => '03295ec1641924349',
            7 => '07d02ee1eeb0c996c',
            8 => '026419edf88a9e928',
            9 => '0a16c2295ef80ff63',
            10 => '00b8c4dc8b8281a68',
            11 => '0ee02acd56a52998e'
        ],
        'us-east-2' =>
        [
            1 => '0443305dabd4be2bc',
            2 => '0ba62214afa52bec7',
            3 => '0f052119b3c7e61d1',
            4 => '00399ec92321828f5',
            5 => '0b9064170e32bde34',
            6 => '0f25b344af3f73199',
            7 => '089fe97bc00bff7cc',
            8 => '0fd58fa2d203e1826',
            9 => '04aa88aebb9fefd83',
            10 => '026e4c271f0e37101',
            11 => '0b9064170e32bde34'
        ],
        'us-west-1' => [
            1 => '04b6c97b14c54de18',
            2 => '054965c6cd7c6e462',
            3 => '05c558c169cfe8d99',
            4 => '0d382e80be7ffdae5',
            5 => '07b068f843ec78e72',
            6 => '0f6c23a2bfb63fbf1',
            7 => '0528712befcd5d885',
            8 => '0d03e644f7fb93689',
            9 => '019a580744fe62a8a',
            10 => '03e0d23f2ad57296b',
            11 => '0ce448b1704085256'
        ],
        'us-west-2' => [
            1 => '083ac7c7ecf9bb9b0',
            2 => '0b28dfc7adc325ef4',
            3 => '0174313b5af8423d7',
            4 => '03d5c68bab01f3496',
            5 => '090717c950a5c34d3',
            6 => '027bdf1182290ac39',
            7 => '0c7ea5497c02abcaf',
            8 => '079e60aa091ba0c80',
            9 => '0a6a14a93c116c949',
            10 => '080387efdab3c4ce3',
            11 => '0dd273d94ed0540c0'
        ],
        'af-south-1' => [
            1 => '0edec67949fd25461',
            2 => '0cf6249186288c4f0',
            3 => '0950dcf60f02f2731',
            4 => '08a4b40f2fe1e4b35',
            5 => '0e93b3dda12646545',
            6 => '0f0f50dd360190a48',
            7 => '0db145f13ff800d7b',
            8 => '07a41516ad0473ee0',
            9 => '09b04716343c2fb82',
            10 => '0107795124f2d8fa0',
            11 => '0becdd21957bf5764'
        ],
        'ap-east-1' => [
            1 => '0a2115f8cc0a3956b',
            2 => '0ec87970e52e19867',
            3 => '0b4017973f2328b15',
            4 => '0b215afe809665ae5',
            5 => '0e0eb5ff358583007',
            6 => '0afc028f9d09e9096',
            7 => '0b94935e67a9d50ff',
            8 => '013f864ad81b818d1',
            9 => '0937793114349c8fe',
            10 => '0e8167323bedd14c7',
            11 => '013f864ad81b818d1'
        ],
        'ap-south-1' => [
            1 => '04db49c0fb2215364 ',
            2 => '06a0b4e3b7eb7a300',
            3 => '0b3acf3edf2397475',
            4 => '0c1a7f89451184c8b ',
            5 => '04bde106886a53080',
            6 => '0655793980c0bf43f',
            7 => '059a9b1093495222c ',
            8 => '01cb77abecc7c4e46',
            9 => '09478f130aea32cae',
            10 => '0a1cd14ebd0d451f3',
            11 => '0860c9429baba6ad2'
        ],
        'ap-northeast-1' => [
            1 => '09ebacdc178ae23b7',
            2 => '0bccc42bba4dedac1',
            3 => '0cbc0209196a8063b',
            4 => '0df99b3a8349462c6',
            5 => '0fe22bffdec36361c',
            6 => '0578730a012dc2185',
            7 => '0ac97798ccf296e02',
            8 => '0eb2456db7dcb6b74',
            9 => '098fdf4b757142f57',
            10 => '08fa185187cfc1b1d',
            11 => '0d53808c8c345ed07'
        ],
        'ap-northeast-2' => [
            1 => '0a0de518b1fc4524c',
            2 => '0bb1758bf5a69ca5c',
            3 => '097fc5cd098dd20d5',
            4 => '04876f29fd3a5e8ba',
            5 => '0ba5cd124d7a79612',
            6 => '0bbac56d527e97165',
            7 => '0b4a713d12660d96c',
            8 => '008670f1f560c49eb',
            9 => '057f06c1cf1880242',
            10 => '07cfc024697bc4257',
            11 => '08508144e576d5b64'
        ],
        'ap-northeast-3' => [
            1 => '0e787554e61105680',
            2 => '066136c75998a0ef3',
            3 => '001f3df5d901d08a4',
            4 => '0001d1dd884af8872',
            5 => '092faff259afb9a26',
            6 => '0c59e5b2f7d091400',
            7 => '00f09455cf36d2e08',
            8 => '041a84f08e0888f8b',
            9 => '0be2514b27721383b',
            10 => '08b6e5de97992d2ce',
            11 => '0eab055d8530dd456'
        ],
        'ap-southeast-1' => [
            1 => '0f511ead81ccde020',
            2 => '0d6ba217f554f6137',
            3 => '03e8d3c5c16f119bb',
            4 => '0d058fe428540cd89',
            5 => '055147723b7bca09a',
            6 => '0528a50f7af604723',
            7 => '01aa83ab14b00e516',
            8 => '01a4b62d68a172014',
            9 => '0c551da7ea785e03e',
            10 => '03c3f7b88cdd97d4e',
            11 => '0f9d733050c9f5365'
        ],
        'ap-southeast-2' =>  [
            1 => '0aab712d6363da7f9',
            2 => '0a443decce6d88dc2',
            3 => '0e413a9954960d83a',
            4 => '0567f647e75c7bc05',
            5 => '0f39d06d145e9bb63',
            6 => '02cfb42ea074403a2',
            7 => '0d2f34c92aa48cd95',
            8 => '03808957a3743a18c',
            9 => '0619f356267f93831',
            10 => '0ccd47adc54889e25',
            11 => '09b1eb4f62e1813d0'
        ],
        'ca-central-1' =>  [
            1 => '02f84cf47c23f1769',
            2 => '0277fbe7afa8a33a6',
            3 => '0d8c9795f4f9f51c0',
            4 => '0801628222e2e96d6',
            5 => '0e28822503eeedddc',
            6 => '00047d3b22cea93fe',
            7 => '01c592dda54e43c1a',
            8 => '02de9053ad1e93be1',
            9 => '02ed5558cc54c6d19',
            10 => '04789d1f13e58441e',
            11 => '0d833a36f4aa82e6b'
        ],
        'eu-central-1' =>  [
            1 => '0453cb7b5f2b7fca2',
            2 => '06ec8443c2a35b0ba',
            3 => '09e8a19c9eda495b3',
            4 => '05f7491af5eef733a',
            5 => '0b1deee75235aa4bb',
            6 => '0eaf9acc4fdb4c39e',
            7 => '0245697ee3e07e755',
            8 => '0442afe916e31ce94',
            9 => '0cf7d5567ccb522db',
            10 => '0a1ec1b4db7ac2769',
            11 => '0a86f18b52e547759'
        ],
        'eu-west-1' =>  [
            1 => '02b4e72b17337d6c1',
            2 => '0ec23856b3bad62d3',
            3 => '00b5dfb1b867959fd',
            4 => '0a8e758f5e873d1c1',
            5 => '0943382e114f188e8',
            6 => '0acec5a529be6b35a',
            7 => '0874dad5025ca362c',
            8 => '04751cbf3ba58fdee',
            9 => '0e13a3d55ca209694',
            10 => '0e121e5eb3d3da758',
            11 => '038d7b856fe7557b3'
        ],
        'eu-west-2' =>  [
            1 => '0d26eb3972b7f8c96',
            2 => '0ad8ecac8af5fc52b',
            3 => '0d7db5fc4b5075b0d',
            4 => '0194c3e07668a7e36',
            5 => '09a56048b08f94cdf',
            6 => '033500b90ed9aa25b',
            7 => '050949f5d3aede071',
            8 => '06cd4571fc78cac8a',
            9 => '0ed58cbcc70e00317',
            10 => '0b75abf94620286c5',
            11 => '066ca69d74f59fe1d'
        ],
        'eu-south-1' =>  [
            1 => '0fce326033a239d55',
            2 => '062f3f95a3f9375aa',
            3 => '08cef65729b7c8850',
            4 => '018f430e4f5375e69',
            5 => '04bf32716a9886470',
            6 => '02bf625e4133d0988',
            7 => '0257e70b7c6db1498',
            8 => '0888a20c450daa4f4',
            9 => '0fcd3a71f2289e989',
            10 => '0cfbaeb95444a6cb1',
            11 => '063c648dab7687f2b'
        ],
        'eu-west-3' =>  [
            1 => '0d49cec198762b78c',
            2 => '08755c4342fb5aede',
            3 => '0f79604849d0fcaab',
            4 => '0f7cd40eac2214b37',
            5 => '06602da18c878f98d',
            6 => '0a37e1926c5d1d2ab',
            7 => '04e905a52ec8010b2',
            8 => '0bf9fcd71ad51c7e2',
            9 => '08e04afac17229274',
            10 => '03dee71cffe3248d2',
            11 => '08be160f24365b105'
        ],
        'eu-north-1' =>  [
            1 => '0d441f5643da997cb',
            2 => '0baa9e2e64f3c00db',
            3 => '00d7bb1aabce7d22c',
            4 => '0ff338189efb7ed37',
            5 => '0afad43e7d620260c',
            6 => '0e857b3d3da49fb4b',
            7 => '0813b14494048969c',
            8 => '0ffe1a1907724392c',
            9 => '0c40f38aa56933928',
            10 => '0fb2e711d02229216',
            11 => '06a87e44ba65501d1'
        ],
        'me-south-1' =>  [
            1 => '004b77593bd476317',
            2 => '05c34053888834402',
            3 => '095711532f1d50122',
            4 => '0eddb8cfbd6a5f657',
            5 => '00a33a0976ef4d65f',
            6 => '0076cf5e1827b4f6f',
            7 => '08ca9d63b8d4ec276',
            8 => '0310f9fedbb58bdde',
            9 => '0ac41ee0ea8d3ea5d',
            10 => '04c4856367cac9e56',
            11 => '091a3b1243690debd'
        ],
        'sa-east-1' =>  [
            1 => '0f8243a5175208e08',
            2 => '0c2485d67d416fc4f',
            3 => '02777cd0ce58a1847',
            4 => '054a31f1b3bf90920',
            5 => '05aa753c043f1dcd3',
            6 => '0e6a2f1d807de5d97',
            7 => '02e2a5679226e293c',
            8 => '02ed4c6b36e7787e3',
            9 => '03b49fb2a2598fdb3',
            10 => '0eec7bffc1e0ef2e6',
            11 => '08c8994979459a12a'
        ],
    ];

    $amis_ARM64 = [
        'us-east-1' =>
        [
            1 => '06cf15d6d096df5d2',
            2 => '01fc429821bf1f4b4',
            3 => '05f2f5f76d89313bb',
            4 => '00d1ab6b335f217cf',
            5 => '08353a25e80beea3e',
            7 => '08b2293fdd2deba2a',
            11 => '0e011417bd70948da'
        ],
        'us-east-2' => [
            1 => '0806cc3ac66515671',
            2 => '09f8674883d0ad6b8',
            3 => '0b99ca359a84941ee',
            4 => '08e6b682a466887dd',
            5 => '026141f3d5c6d2d0c',
            7 => '0071048c60844169f',
            11 => '0bff25b43a4479334'
        ],
        'us-west-1' => [
            1 => '03b5f921df0166ba8',
            2 => '05f88a4bcb91f4ea7',
            3 => '05e206de142efa13a',
            4 => '0c58a45b4cecce02c',
            5 => '0437ad1b6a022fafe',
            7 => '034231a42fe64a311',
            11 => '0daf1443f6adffe0d'
        ],
        'us-west-2' => [
            1 => '0a12b6f54cdcb8114',
            2 => '07465754c59218cdb',
            3 => '0134c4ce50d03117f',
            4 => '09d9c897fc36713bf',
            5 => '0327006c87b23e535',
            7 => '0983cbbef7dc44d32',
            11 => '03ac21435677d3cb3'
        ],
        'af-south-1' => [],
        'ap-east-1' => [
            1 => '04c9c40ab5998bac1',
            2 => '047f2232912795fcf',
            3 => '0748db038a2205f21',
            4 => '0e23b27993ca4238c',
            5 => '077f02bac13982e77',
            7 => '0eb19c34af7a0e2e3',
            11 => '0e4edb3648cd12c6b'
        ],
        'ap-south-1' => [
            1 => '0086e63bfa49c3b49',
            2 => '0cbe04a3ce796c98e',
            3 => '0ab71076ab9b53b0d ',
            4 => '0d18acc6e813fd2e0',
            5 => '04f6f742e1d9012e3 ',
            7 => '03b05547d5c0bbfe4',
            11 => '03ab5f3b31d5ee063 '
        ],
        'ap-northeast-1' => [
            1 => '06b31a9cee8dfac33',
            2 => '0cdc4f61f73af4679',
            3 => '00aa2ab20df3d86ec',
            4 => '076d8ebdd0e1ec091',
            5 => '078fe86fa4c333481',
            7 => '0ed400c2ea06a311c',
            11 => '0f5ff319c5b9ca253'
        ],
        'ap-northeast-2' =>   [
            1 => '03daae882e94f15e0',
            2 => '0ae6cf93168b8df72',
            3 => '06fb0a38fed3dcdec',
            4 => '0815e517ed50fd3f4',
            5 => '08b051fc14e6c551e',
            7 => '0b389b781fc0edd74',
            11 => '0e1377d9004846fd8'
        ],
        'ap-northeast-3' =>   [],
        'ap-southeast-1' =>   [
            1 => '0846d2a9ef9746f7f',
            2 => '0724377cd34a397c2',
            3 => '0cf02f1c3b1707b34',
            4 => '077adae4d983338da',
            5 => '062e2ec9a8bfa02d6',
            7 => '0ebff430e49cf2588',
            11 => '0ccbe0d11813b32c0'
        ],
        'ap-southeast-2' => [
            1 => '02dc2e45afd1dc0db',
            2 => '06aea65a96a49fc05',
            3 => '0fec5879d3ec972f1',
            4 => '07aa5ef6af56f8da2',
            5 => '0ac142889d7d97567',
            7 => '0acb014ad41e02ed9',
            11 => '04bd83af08a494884'
        ],
        'ca-central-1' => [
            1 => '0a6f216528378ffca',
            2 => '0ee8fe29139ee1481',
            3 => '07a449386fa1e3715',
            4 => '0994658be3d2178e0',
            5 => '07ba772924ecc689f',
            7 => '0076199039cf582d4',
            11 => '01f99cb8d0ab084d5'
        ],
        'eu-central-1' => [
            1 => '03c10046b8071fff5',
            2 => '0b095f5954d2592ad',
            3 => '00f2e594abc782530',
            4 => '08f11f4114f566d1a',
            5 => '01bced7e7239dbd82',
            7 => '0886e2450125a1f08',
            11 => '0ec6287a810b5b6e9'
        ],
        'eu-west-1' => [
            1 => '04b149cd223547c24',
            2 => '049b4f7f9e1b8f47c',
            3 => '0197db506d3fff6c7',
            4 => '09e0d6fdf60750e33',
            5 => '07648455888dfc767',
            7 => '02821705778353bd1',
            11 => '0110a207d45daf145'
        ],
        'eu-west-2' =>  [
            1 => '0da41f3e3c8c8c465',
            2 => '0dea0cf236484a796',
            3 => '0fdd4500e38324e55',
            4 => '0960f1036d6edacf5',
            5 => '0fa14d6dc09479348',
            7 => '0d7a4d4a21a9f8b1f',
            11 => '07438ed9014cde68f'
        ],
        'eu-south-1' =>  [
            1 => '0ca87956296d64df4',
            2 => '0e8479b3220b4a676',
            3 => '0a93e354e2daea4e7',
            4 => '0c4fa595047444538',
            5 => '0c1e0125bac830d95',
            7 => '0b44a6dbea0dd48c3',
            11 => '06ab6cebcf76830bf'
        ],
        'eu-west-3' =>  [
            1 => '0ba32d77d953cc750',
            2 => '0acdec03cead7ae33',
            3 => '03032f57edc353374',
            4 => '082e545ca343178ad',
            5 => '0dc556c21e5099c75',
            7 => '04e905a52ec8010b2',
            11 => '07e72e53f4191740f'
        ],
        'eu-north-1' =>  [
            1 => '0223e7391593c3843',
            2 => '0d81fd43a83bd80b1',
            3 => '0eca0b363e8642aa4',
            4 => '069568e10baf004ec',
            5 => '00320b1b198c6f31e',
            7 => '0822bab54716b8c2a',
            11 => '0e5f99bd9d7da8a42'
        ],
        'me-south-1' =>  [
            1 => '01f37d5d6bdaa5b1b',
            2 => '07da2f202d6ae3673',
            3 => '0d9ecdaabe1e8fbfa',
            4 => '065ec9c3eab78698f',
            5 => '0cf545c9ae2347d1e',
            7 => '0230b2fe0f1eb96a3',
            11 => '01bf79f41a96e8021'
        ],
        'sa-east-1' =>  [
            1 => '06cdeb6aceb197446',
            2 => '09ae0a24dd3aca40c',
            3 => '0e60220a8ad6d35b0',
            4 => '0c9c4f3290bbba7b4',
            5 => '0bd03f2c1034d9845',
            7 => '0f40bb5d33d92f0f5',
            11 => '0ec4e818278c35a2e'
        ],
    ];

    return $is_ARM64 ? 'ami-' . $amis_ARM64[$region][$index] : 'ami-' . $amis_amd64[$region][$index];
}
